﻿using System;

namespace ordenamiento
{
    class Program
    {
        static void Main(string[] args)
        {
            string metodo = "Metodo", valor1 = "1000", valor2 = "10000", valor3 = "20000";

            var calculo = new Contador();

            Console.WriteLine($"\n{metodo.PadRight(15)} {valor1.PadRight(10)} {valor2.PadRight(10)} {valor3.PadRight(10)}");
            Console.WriteLine("--------------------------------------------");

            calculo.ContadorBurbuja();
            calculo.ContadorSeleccion();
            calculo.ContadorInsercion();
            calculo.ContadorQuicksort();

            Console.WriteLine();
        }
    }
}