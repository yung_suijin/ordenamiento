using System;

namespace ordenamiento
{
    internal class FuncionesOrdenamiento
    {
        public void Burbuja(int[] arreglo, int n)
        {
            if (n == 1)
                return;

            for (int i = 0; i < n - 1; i++)
                if (arreglo[i] > arreglo[i + 1])
                {
                    int temporal = arreglo[i];
                    arreglo[i] = arreglo[i + 1];
                    arreglo[i + 1] = temporal;
                }

            Burbuja(arreglo, n - 1);
        }

        public void Quicksort(int[] arreglo, int primero, int ultimo)
        {
            Func<int[], int, int, int> calcularPivote = null;
            calcularPivote = (arreglo, primero, ultimo) =>
            {
                int pivote = arreglo[ultimo];

                int i = (primero - 1);
                for (int j = primero; j < ultimo; j++)
                {
                    if (arreglo[j] < pivote)
                    {
                        i++;
                        int temporal = arreglo[i];
                        arreglo[i] = arreglo[j];
                        arreglo[j] = temporal;
                    }
                }

                int temporal1 = arreglo[i + 1];
                arreglo[i + 1] = arreglo[ultimo];
                arreglo[ultimo] = temporal1;

                return i + 1;
            };

            if (primero < ultimo)
            {
                int index = calcularPivote(arreglo, primero, ultimo);

                Quicksort(arreglo, primero, index - 1);
                Quicksort(arreglo, index + 1, ultimo);
            }
        }

        public void Seleccion(int[] arreglo, int i, int n)
        {
            Action<int[], int, int> cambiar = (arreglo, i, j) =>
            {
                int temporal = arreglo[i];
                arreglo[i] = arreglo[j];
                arreglo[j] = temporal;
            };

            int valorMinimo = i;
            
            for (int j = i + 1; j < n; j++)
            {
                if (arreglo[j] < arreglo[valorMinimo])
                    valorMinimo = j; 
            }

            cambiar(arreglo, valorMinimo, i);

            if (i + 1 < n)
                Seleccion(arreglo, i + 1, n);
        }

        public void Insercion(int[] arreglo, int n)
        {
            if (n <= 1)
                return;

            Insercion(arreglo, n - 1);

            int ultimo = arreglo[n - 1];
            int j = n - 2;

            while (j >= 0 && arreglo[j] > ultimo)
            {
                arreglo[j + 1] = arreglo[j];
                j--;
            }
            arreglo[j + 1] = ultimo;
        }
    }
}