using System;

namespace ordenamiento
{
    internal class Contador
    {
        public int x = 1000, y = 10000, z = 20000;
        ValoresRandom random = new ValoresRandom();
        FuncionesOrdenamiento funcion = new FuncionesOrdenamiento();

        public void ContadorBurbuja()
        {
            string metodo = "Burbuja";

            int[] arreglo = random.GeneradorRandom(x);

            var contador1 = System.Diagnostics.Stopwatch.StartNew();
            funcion.Burbuja(arreglo, arreglo.Length);
            contador1.Stop();
            var burbuja1 = contador1.ElapsedMilliseconds;

            int[] arreglo2 = random.GeneradorRandom(y);

            var contador2 = System.Diagnostics.Stopwatch.StartNew();
            funcion.Burbuja(arreglo2, arreglo2.Length);
            contador2.Stop();
            var burbuja2 = contador2.ElapsedMilliseconds;

            int[] arreglo3 = random.GeneradorRandom(z);

            var contador3 = System.Diagnostics.Stopwatch.StartNew();
            funcion.Burbuja(arreglo3, arreglo3.Length);
            contador3.Stop();
            var burbuja3 = contador3.ElapsedMilliseconds;

            Console.WriteLine($"{metodo.PadRight(15)} {burbuja1.ToString().PadRight(10)} {burbuja2.ToString().PadRight(10)} {burbuja3.ToString().PadRight(10)} ");
        }

        public void ContadorInsercion()
        {
            string metodo = "Insercion";

            int[] arreglo = random.GeneradorRandom(x);

            var contador1 = System.Diagnostics.Stopwatch.StartNew();
            funcion.Insercion(arreglo, arreglo.Length);
            contador1.Stop();
            var insercion1 = contador1.ElapsedMilliseconds;

            int[] arreglo2 = random.GeneradorRandom(y);

            var contador2 = System.Diagnostics.Stopwatch.StartNew();
            funcion.Insercion(arreglo2, arreglo2.Length);
            contador2.Stop();
            var insercion2 = contador2.ElapsedMilliseconds;

            int[] arreglo3 = random.GeneradorRandom(z);

            var contador3 = System.Diagnostics.Stopwatch.StartNew();
            funcion.Insercion(arreglo3, arreglo3.Length);
            contador3.Stop();
            var insercion3 = contador3.ElapsedMilliseconds;

            Console.WriteLine($"{metodo.PadRight(15)} {insercion1.ToString().PadRight(10)} {insercion2.ToString().PadRight(10)} {insercion3.ToString().PadRight(10)} ");
        }

        public void ContadorSeleccion()
        {
            string metodo = "Seleccion";

            int[] arreglo = random.GeneradorRandom(x);

            var contador1 = System.Diagnostics.Stopwatch.StartNew();
            funcion.Seleccion(arreglo, 0, arreglo.Length);
            contador1.Stop();
            var seleccion1 = contador1.ElapsedMilliseconds;

            int[] arreglo2 = random.GeneradorRandom(y);

            var contador2 = System.Diagnostics.Stopwatch.StartNew();
            funcion.Seleccion(arreglo2, 0, arreglo2.Length);
            contador2.Stop();
            var seleccion2 = contador2.ElapsedMilliseconds;

            int[] arreglo3 = random.GeneradorRandom(z);

            var contador3 = System.Diagnostics.Stopwatch.StartNew();
            funcion.Seleccion(arreglo3, 0, arreglo3.Length);
            contador3.Stop();
            var seleccion3 = contador3.ElapsedMilliseconds;

            Console.WriteLine($"{metodo.PadRight(15)} {seleccion1.ToString().PadRight(10)} {seleccion2.ToString().PadRight(10)} {seleccion3.ToString().PadRight(10)} ");
        }

        public void ContadorQuicksort()
        {
            string metodo = "Quicksort";

            int[] arreglo = random.GeneradorRandom(x);

            var contador1 = System.Diagnostics.Stopwatch.StartNew();
            funcion.Quicksort(arreglo, 0, arreglo.Length - 1);
            contador1.Stop();
            var quicksort1 = contador1.ElapsedMilliseconds;

            int[] arreglo2 = random.GeneradorRandom(y);

            var contador2 = System.Diagnostics.Stopwatch.StartNew();
            funcion.Quicksort(arreglo2, 0, arreglo2.Length - 1);
            contador2.Stop();
            var quicksort2 = contador2.ElapsedMilliseconds;

            int[] arreglo3 = random.GeneradorRandom(z);

            var contador3 = System.Diagnostics.Stopwatch.StartNew();
            funcion.Quicksort(arreglo3, 0, arreglo3.Length - 1);
            contador3.Stop();
            var quicksort3 = contador3.ElapsedMilliseconds;

            Console.WriteLine($"{metodo.PadRight(15)} {quicksort1.ToString().PadRight(10)} {quicksort2.ToString().PadRight(10)} {quicksort3.ToString().PadRight(10)} ");
        }
    }
}