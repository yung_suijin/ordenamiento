using System;
using System.Collections.Generic;
using System.Linq;

namespace ordenamiento
{
    internal class ValoresRandom
    {
        public int[] GeneradorRandom(int valor)
        {
            var random = new Random();

            IEnumerable<int> RandomSequence(Random random)
            {
                while (true)
                {
                    yield return random.Next();
                }
            }

            int[] arreglo = RandomSequence(random).Distinct().Take(valor).ToArray();

            return arreglo;
        }
    }
}